# Application startup 

main.ts
  app.module 
    ConfigModule              ; Create a store for env vars 
    DatabaseModule            ; Setup the database 
    Auth0Module               ; Setup auth0 
    UserModule                ; Setup user
      fetch user from auth0 

    BalanceModule             ; Setup balance
    AdminModule               ; Setup admin
    MessageModule             ; Setup message
    PaymentModule             ; Setup payment
    TransactionModule         ; Setup transaction
    // VoucherModule          ; Setup voucher
    ScheduleModule.forRoot()  ; 
    TasksModule               ; Setup tasks (cron)


## Sync users from auth0 at startup 
  All lifecycle hooks run asynchronously, regardless of their importing order. If you want to affect an instantiation process (for example, a provider no. 1 has to be resolved before provider no. 2) you need to create an async component: https://docs.nestjs.com/ (Fundamentals -> Async Components)
  
  https://stackoverflow.com/questions/48908742/nestjs-resolve-custom-startup-dependency
  https://docs.nestjs.com/fundamentals/custom-providers

## Module dependency injection
  https://stackoverflow.com/questions/50586417/nestjs-custom-provider-inject-cant-resolve-dependencies-of-the-usefactory
  