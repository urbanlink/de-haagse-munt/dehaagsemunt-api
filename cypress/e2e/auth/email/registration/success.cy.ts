import { gen } from 'cypress/helpers/gen';

context('Registration success with email profile', () => {
  before(() => {
    cy.clearCookies();
    cy.visit(`${Cypress.env('kratosUrl')}/ui/registration`);
  });

  it('should sign up and be logged in', () => {
    const email = gen.email();
    const password = gen.password();

    cy.get('input[name="traits.email"]').type(email);
    cy.get('input[name="password"]').type(password);

    cy.get('[name="method"][value="password"]').click();
    cy.get('[name="method"][value="password"]:disabled').should('not.exist');
  });
});
