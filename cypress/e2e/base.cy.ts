describe('check API status', () => {
  it('verify request returns JSON', () => {
    cy.request('/').its('status').should('be.equal', 200);
  });
});
