export const email = () => Math.random().toString(36) + '@urbanlink.nl';
export const blockedEmail = () => Math.random().toString(36) + '_blocked' + '@urbanlink.nl';

export const password = () => Math.random().toString(36);

export const gen = {
  email,
  blockedEmail,
  password,
};
