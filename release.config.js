module.exports = {
  branches: [{ name: 'main' }, { name: '+([0-9])?(.{+([0-9]),x}).x' }],
  repositoryUrl: 'https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api',
  debug: 'true',
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    [
      '@semantic-release/npm',
      {
        npmPublish: false,
      },
    ],
    [
      '@semantic-release/gitlab',
      {
        gitlabUrl: 'https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api',
      },
    ],
    [
      '@semantic-release/changelog',
      {
        changelogFile: 'CHANGELOG.md',
      },
    ],
    [
      '@semantic-release/git',
      {
        assets: ['package.json', 'yarn.lock', 'CHANGELOG.md'],
        message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
      },
    ],
  ],
};
