import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    env: {
      kratosUrl: 'https://blissful-cartwright-na8bkje4u2.projects.oryapis.com',
    },
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
