## [0.6.17](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.16...v0.6.17) (2020-08-30)


### Performance Improvements

* **ci:** put node_modules inside dist on prod ([2db5b00](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/2db5b00031eaa65cacf55280409cf07181503974))

## [0.6.16](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.15...v0.6.16) (2020-08-30)


### Performance Improvements

* **ci:** test files in docker container ([8c8bec5](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/8c8bec5565da5e8fca42c2be0d4ca9ca6e2e5302))

## [0.6.15](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.14...v0.6.15) (2020-08-30)


### Performance Improvements

* **ci:** copy package.json in dockerfile ([959a40b](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/959a40ba6ff7be3a7b6fed8fad655fe621ce1a87))

## [0.6.14](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.13...v0.6.14) (2020-08-30)


### Performance Improvements

* **ci:** build production packages in docker ([0b49a11](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/0b49a11601ec0b63cafc5298de6e995bb5a246e5))

## [0.6.13](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.12...v0.6.13) (2020-08-30)


### Performance Improvements

* **ci:** update dockerignore ([9b1d265](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/9b1d26551ed7c195ccc3baaa3ea21e8d563694e7))

## [0.6.12](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.11...v0.6.12) (2020-08-30)


### Performance Improvements

* **ci:** fix docker build ([23d3b86](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/23d3b8655c2a23ab29ac263d657b16b91a29e194))

## [0.6.11](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.10...v0.6.11) (2020-08-30)


### Performance Improvements

* **ci:** add cache to build job ([e25f970](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/e25f970dff018e371786da2002c428ca1eed0edb))

## [0.6.10](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.9...v0.6.10) (2020-08-30)


### Performance Improvements

* **ci:** copy node modules in dockerfile ([5094947](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/509494762d3e56741b6880b44de51afb7ea79ea8))

## [0.6.9](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.8...v0.6.9) (2020-08-30)


### Performance Improvements

* **ci:** update deploy job ([4b75732](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/4b75732747f8d3a4fd23dd3b6bfa83a688c675db))

## [0.6.8](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.7...v0.6.8) (2020-08-30)


### Performance Improvements

* **ci:** update deploy commands ([6f43f54](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/6f43f54614fb6bb9f034fd279d89c062b4861329))

## [0.6.7](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.6...v0.6.7) (2020-08-30)


### Performance Improvements

* **ci:** build in ci, not docker ([31c00bf](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/31c00bfb6cd5e24b4448640469cd42f603d24929))

## [0.6.6](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.5...v0.6.6) (2020-08-29)


### Performance Improvements

* **ci:** build in dockerfile ([555745a](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/555745a17d36a178e8faf2bbb5c7a3150864ea6b))

## [0.6.5](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.4...v0.6.5) (2020-08-29)


### Performance Improvements

* **ci:** update gitlab-ci ([f0f6c64](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/f0f6c645030990b01de3a5f87b3fab9cecb46d2d))

## [0.6.4](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.3...v0.6.4) (2020-08-29)


### Performance Improvements

* **ci:** update dockerfile ([02f1fe3](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/02f1fe32408f0d420bd3b5e6ba042bd1a80aa6ff))

## [0.6.3](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.2...v0.6.3) (2020-08-29)


### Bug Fixes

* **sentry:** update sentry settingd ([6eccd5c](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/6eccd5c8c54894ce160efe437afbe39df705ebcd))


### Performance Improvements

* **ci:** update dockerfile ([0282812](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/0282812301af24d26a100b373b89cf65fdc2ea92))
* **core:** update packages ([d9cd1ae](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/d9cd1ae24bdb19bdbe1611962724aee31d63c8a3))

## [0.6.2](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.1...v0.6.2) (2020-08-29)


### Performance Improvements

* **ci:** use artifacts add build_image ([fa6c052](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/fa6c0529c0f54472f501d584968951dc90a58850))

## [0.6.1](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/compare/v0.6.0...v0.6.1) (2020-08-29)


### Bug Fixes

* **auth:** update auth mnodule ([d310ae9](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/d310ae94d95fe0566a9b3051797c5a768451c5e0))
* **ci:** update config ([3839482](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/3839482d6829ced6046815574b34fc347c2b7137))
* **db:** add database migrations ([579571a](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/579571a5429967755254a6e5d4731216ff9053af))


### Performance Improvements

* **ci:** add semantic release packages ([8485640](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/8485640f44b3e5f645b44717286c46a5c2536b43))
* **ci:** check release token ([2391d59](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/2391d59d976dae744ba6490b960dcee8ec189f54))
* **ci:** fix ci config ([b81addb](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/b81addb7cdf4967f4b40926ba9faa3c461321396))
* **ci:** fix test job ([bc9d482](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/bc9d482da87ae8f10df08b1223c55170b6d83626))
* **ci:** use node-lts ([c8bdab5](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/c8bdab59e7583ff8e3277dd4b001299cd0934300))
* **ci:** use node:lts for release job ([e743677](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/e743677067deab12c4f93e9bbd553668b202a00d))
* **core:** update app init ([778ce32](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/778ce3276ddbbade18427d4943d5c71d6d12f884))
* **core:** update package scripts ([8cd3d02](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/8cd3d028150fc3dc741c88358e9d73de0d34518e))
* **core:** update packages ([5ec0b41](https://gitlab.com/urbanlink/de-haagse-munt/dehaagsemunt-api/commit/5ec0b41aebed0b00c8cc3ba5b645c438effc1a42))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.6.0](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/compare/v0.5.1...v0.6.0) (2020-06-13)


### Features

* update to the latest version ([dc0a6dc](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/dc0a6dc08626f4ec8e963cc83c957e34b56c0a9a))
* **scripts:** add watcher script via nodemon ([c0c6e49](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/c0c6e4988be2244fe7cb0122211d3e277b137063))


### Bug Fixes

* add libs and apps to lint script ([77ea064](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/77ea06457ee1e8731407e050e4d72ae27fb3a40f))
* bump versions in package.json ([a0b76b0](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/a0b76b0f707fe8b92f5ba796b58fd98f486a3bf2))
* remove redundant deprecate dependency ([833491e](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/833491eedd8f858a417159a78d4eee219e049359))
* remove tsconfig paths in nodemon, remove prefixes ([30cc388](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/30cc388d31352c9beb754d672fea36402082146a))
* update package-lock.json ([15adecd](https://gitlab.com/de-haagse-munt/dehaagsemunt-api/commit/15adecd3a14d8afed93783a1942ab1254c3e5c20))
