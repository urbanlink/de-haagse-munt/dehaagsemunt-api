FROM node:lts-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

# Copy build files from previous job
COPY dist ./
RUN ls -la
# Install production modules
COPY package.json yarn.lock ./
RUN yarn install --prod

CMD ["node", "main"]
