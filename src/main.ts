import { NestFactory } from '@nestjs/core';
import { AppModule } from '@app/app.module';
import { Logger, ValidationPipe, } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as Sentry from '@sentry/node';
import { SentryInterceptor } from '@utils/sentry.interceptor';
import { NestExpressApplication } from '@nestjs/platform-express';
import { json, urlencoded } from 'express';
import cookieParser from 'cookie-parser';
import { environment } from './environments/environment.prod';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));

  app.use(cookieParser());

  if (environment.sentryDsn) {
    Sentry.init({
      dsn: environment.sentryDsn,
      debug: !environment.production,
      environment: environment.env,
    });
  }

  app.useGlobalInterceptors(new SentryInterceptor());

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  const config: ConfigService = app.get(ConfigService);
  app.enableCors({
    origin: config.get('FRONTEND_URL'),
    credentials: true,
  });

  const port = environment.port;

  const swaggerConfig = new DocumentBuilder()
    .setTitle('voOot API Docs')
    .setDescription('The voOot API docs')
    .setVersion('1.0')
    .addTag('voOot')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);

  await app.listen(environment.port, () => {
    Logger.log(`Listening at ${environment.backendUrl} on ${environment.port}`);
  });
}

bootstrap();
