export class MessageCountDto {
  user_id?: number;
  user_auth0?: string;
  user_email?: string;
  user_nickname?: string;
  user_last_email_digest?: string;
  message_count?: number;
}
