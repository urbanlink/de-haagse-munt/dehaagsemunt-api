import { User } from '@prisma/client';

export class MessageQueryDto {
  user?: User;
  sub?: string;
  messageId?: number;
  limit?: number;
  order?: number;
  read_status?: boolean;
  delete_status?: boolean;
}
