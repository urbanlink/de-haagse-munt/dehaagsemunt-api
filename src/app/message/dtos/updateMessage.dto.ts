import { message_level_enum } from '@prisma/client';

export class UpdateMessageDto {
  subject?: string;
  body?: string;
  type?: string;
  level?: message_level_enum;
  emailed_status?: boolean;
  read_status?: boolean;
  delete_status?: boolean;
}
