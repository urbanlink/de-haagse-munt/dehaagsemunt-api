import { message_level_enum, User } from '@prisma/client';

export class CreateMessageDto {
  user: User;

  subject: string;

  body: string;

  type: string;

  level: message_level_enum;

  emailed_status: boolean;

  read_status: boolean;

  delete_status: boolean;
}
