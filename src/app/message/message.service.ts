import { Logger, Injectable } from '@nestjs/common';
import { PrismaService } from '@app/database/prisma.service';
import { MessageQueryDto } from './dtos/messageQuery.dto';
import { Message } from '@prisma/client';
import { UpdateMessageDto } from './dtos/updateMessage.dto';
import { CreateMessageDto } from './dtos/createMessage.dto';

@Injectable()
export class MessageService {
  private logger = new Logger(MessageService.name);

  constructor(private readonly prismaService: PrismaService) {}

  // Fetch messages for a user
  async getMessages(opts: MessageQueryDto): Promise<Message[]> {
    const { sub } = opts;
    if (!sub) {
      Logger.warn('[ message.service ] - No user provided. ');
    }
    return await this.prismaService.message.findMany();
  }

  // Fetch message for a user
  async getMessage(opts: MessageQueryDto) {
    if (!opts.sub) Logger.warn('[ message.service ] - No user provided');
    if (!opts.messageId) Logger.warn('[ message.service ] - No messageid provided');

    return await this.prismaService.message.findFirst();
  }

  async UpdateMessage(sub: string, mid: number, updateData: UpdateMessageDto) {
    this.logger.log(`Updating message ${mid} for user ${sub} with values ${JSON.stringify(updateData)}`);

    return await this.prismaService.message.findFirst();
  }

  async GetUnsentMessagesByUser(offset: number | null = null): Promise<Message | null> {
    // Set selection date
    offset = offset || 24 * 60 * 60 * 1000 * 1; //1 day;
    const d = new Date();
    d.setTime(d.getTime() - offset);
    d.toISOString();
    // Set query params
    const filters: any = {
      digestDate: d as any,
    };

    return await this.prismaService.message.findFirst();
  }

  async getUnreadCount(sub: string): Promise<any> {
    return await this.prismaService.message.findFirst();
  }

  async createMessage(msgDto: CreateMessageDto): Promise<Message | null> {
    return await this.prismaService.message.findFirst();
  }
}
