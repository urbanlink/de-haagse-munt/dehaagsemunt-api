import {
  Controller,
  Get,
  Put,
  Req,
  Param,
  Body,
  UseInterceptors,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { MessageService } from '@app/message/message.service';
import { SentryInterceptor } from '@utils/sentry.interceptor';
import { Message } from '@prisma/client';
import { UpdateMessageDto } from './dtos/updateMessage.dto';

export interface RequestInterface extends Request {
  user: any;
}

@UseInterceptors(new SentryInterceptor())
@Controller('message')
export class MessageController {
  private readonly logger = new Logger(MessageController.name);

  constructor(private readonly messageService: MessageService) {
    this.logger.log('Initiated');
  }

  /**
   *
   * Find all messages for current user.
   *
   **/
  @Get()
  async findAll(@Req() req: RequestInterface) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: Message[];
    try {
      result = await this.messageService.getMessages({ sub: user.sub });
    } catch (err) {
      return new HttpException('Error fetching message list', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  /**
   *
   * Get the number of unread messages for a user.
   *
   **/
  @Get('unread')
  async getUnreadCount(@Req() req: RequestInterface): Promise<number | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: number;
    try {
      result = await this.messageService.getUnreadCount(user.sub);
    } catch (err) {
      return new HttpException('Error fetching unread message count', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  /**
   *
   * Get a single message for current user.
   *
   **/
  @Get(':id')
  async getMessage(@Param('id') id: string, @Req() req: RequestInterface): Promise<Message | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: Message;
    try {
      result = await this.messageService.getMessage({ sub: user.subm, messageId: Number(id) });
    } catch (err) {
      return new HttpException(`Error fetching message ${id}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  /**
   *
   * Update a single message for current user.
   *
   **/
  @Put(':id')
  async updateMessage(@Param('id') id: number, @Req() req: RequestInterface, @Body() updateData: UpdateMessageDto) {
    // Make sure current user exists
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result;
    try {
      result = await this.messageService.UpdateMessage(user.sub, id, updateData);
    } catch (err) {
      return new HttpException(`Error fetching message ${id}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }
}
