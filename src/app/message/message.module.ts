import { Module } from '@nestjs/common';
import { MessageController } from '@app/message/message.controller';
import { MessageService } from '@app/message/message.service';

@Module({
  controllers: [MessageController],
  providers: [MessageService],
  exports: [MessageService],
})
export class MessageModule {}
