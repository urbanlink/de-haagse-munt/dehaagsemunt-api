import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { NotificationsModule } from '@app/notification/notification.module';
import { EmailModule } from '@app/email/email.module';
import { UserModule } from '@app/user/user.module';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [NotificationsModule, EmailModule, UserModule, TransactionModule],
  providers: [TasksService],
})
export class TasksModule {}
