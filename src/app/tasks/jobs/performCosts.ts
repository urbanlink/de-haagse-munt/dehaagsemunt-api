/**
 *
 * Handle the account costs (monthly charges) and transfer to administration account.
 *
 **/

import { Logger } from '@nestjs/common';

const performCosts = (): boolean => {
  const logger = new Logger('performcosts');
  logger.log('initiate');
  return true;
};

export default performCosts;
