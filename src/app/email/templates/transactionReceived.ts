// module.exports = function(data) {
//     var out = {};
  
//     out.subject = 'Nieuw tegoed ontvangen (De Haagse Munt)';
  
//     var html = '';
//     html += 'Hallo ' + data.recipient.displayname + ',<br><br>';
  
//     html += '<p>Je hebt <strong>' + data.transaction.amount + '</strong> HM ontvangen van <strong>' + data.sender.displayname  + '</strong></p>';
//     // html += '<p>Je balans is nu '+ data.transaction.balance +' HM.</p><br>';
  
//     html += '<p><strong>Transactie details:</strong></p>';
//     html += '<table style="text-align:left">';
//     html += '<tr><td><strong>Van: </strong></td><td>' + data.sender.displayname + '</td></tr>';
//     html += '<tr><td><strong>Aan: </strong></td><td>' + data.recipient.displayname + '</td></tr>';
//     html += '<tr><td><strong>Bedrag: </strong></td><td>' + data.transaction.amount + ' HM</td></tr>';
//     if(data.transaction.description) {
//       html += '<tr><td><strong>Beschrijving: </strong></td><td>' + data.transaction.description + '</td></tr>';
//     }
//     html += '</table>';
//     html += '<br>';
  
//     html += '<p>Bekijk het overzicht van je rekening in je <a href="https://dehaagsemunt.nl/dashboard">dashboard</a>.</p><br></br>';
  
//     html += '<p>De Haagse Munt</p><br><br>';
  
//     out.html = html;
  
  
//     // Plain message
//     let plain = '';
//     plain += 'Hallo ' + data.recipient.displayname + ',\n';
//     plain += '\n';
//     plain += 'Je hebt ' + data.transaction.amount + ' Haagse Munten ontvangen van ' + data.sender.displayname  + '. \n';
//     plain += '\n';
//     plain += 'Transactie details: \n';
//     plain += 'Van: ' + data.sender.displayname + '\n';
//     plain += 'Aan: ' + data.recipient.displayname + ' \n';
//     plain += 'Bedrag: ' + data.transaction.amount + ' HM \n';
//     plain += 'Beschrijving: ' + data.transaction.description + ' \n';
//     plain += '\n';
//     plain += '\n';
//     plain += 'Bekijk het overzicht van je rekening op https://dehaagsemunt.nl/dashboard. \n';
//     plain += '\n';
//     plain += '\n';
  
//     plain += 'De Haagse Munt';
  
//     out.plain = plain;
  
//     return out;
//   };
  