import { MailData } from '@sendgrid/helpers/classes/mail';
import { EmailTemplateData } from '../dto/emailTemplate.dto';

const Template = (data: EmailTemplateData) => {
  const out: MailData = {
    to: data.to,
    subject: `Inleg ontvangen`,
    from: data.from,
    html: `<p>Hallo {{displayname}},</p>
        <p>Je inleg van €${data.payment.amount} bij De Haagse Munt is ontvangen en verwerkt. De Haagse Munten zijn bijgeschreven op je account.</p>
        <p>Veel plezier met de echte Haagse munten.</p>
        <p><a href="https://dehaagsemunt.nl">De Haagse Munt</a></p>`,

    text: `Hallo {{displayname}},\n\n
        Je inleg bij De Haagse Munt is ontvangen en verwerkt. De Haagse Munten zijn bijgeschreven op je account. \n\n
        Veel plezier met de echte Haagse munten.\n\n\n
        <a href="https://dehaagsemunt.nl">De Haagse Munt</a>`,
  };

  return out;
};

export default Template;
