import { Module } from '@nestjs/common';
import { EmailService } from '@app/email/email.service';

@Module({
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
