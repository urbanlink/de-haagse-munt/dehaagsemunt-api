import { Balance, Payment } from '@prisma/client';

export class EmailTemplateData {
  type: string;
  to: string;
  from?: string;
  nickname?: string;
  messageCount?: number;
  transactionsCount?: number;
  currentBalance?: Balance;
  previousBalance?: Balance;
  payment?: Payment;
}
