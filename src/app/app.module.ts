import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { DatabaseModule } from '@app/database/database.module';
import { UserModule } from '@app/user/user.module';
import { BalanceModule } from '@app/balance/balance.module';
import { MessageModule } from '@app/message/message.module';
import { PaymentModule } from '@app/payment/payment.module';
import { TransactionModule } from '@app/transaction/transaction.module';
import { VoucherModule } from '@app/voucher/voucher.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksModule } from '@app/tasks/tasks.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    DatabaseModule,
    UserModule,
    BalanceModule,
    MessageModule,
    PaymentModule,
    TransactionModule,
    VoucherModule,
    ScheduleModule.forRoot(),
    TasksModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
