import { Controller, Post, Body, Req, HttpException, HttpStatus } from '@nestjs/common';
import { ApiCookieAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { VoucherService } from '@app/voucher/voucher.service';
import { RedeemVoucherDto } from '@app/voucher/dto/redeemVoucher.dto';

export interface RequestInterface extends Request {
  user: { kratosId: string };
  key?: string;
}

@ApiCookieAuth()
@ApiTags('voucher')
@Controller('voucher')
export class VoucherController {
  constructor(private readonly voucherService: VoucherService) {}

  @Post('redeem')
  @ApiOperation({ summary: 'Redeem a voucher for current user' })
  @ApiResponse({ status: 200, description: 'Redeem a voucher for current user.' })
  async redeem(@Req() req: RequestInterface, @Body() { key }: RedeemVoucherDto) {
    if (!key) return new HttpException('No key povided', HttpStatus.BAD_REQUEST);

    return await this.voucherService.RedeemVoucher(req.user.kratosId, key);
  }
}
