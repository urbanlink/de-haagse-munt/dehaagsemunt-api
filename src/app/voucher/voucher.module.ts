import { Module } from '@nestjs/common';
import { VoucherController } from '@app/voucher/voucher.controller';
import { VoucherService } from '@app/voucher/voucher.service';
import { UserModule } from '@app/user/user.module';
import { TransactionModule } from '@app/transaction/transaction.module';

@Module({
  imports: [UserModule, TransactionModule],
  controllers: [VoucherController],
  providers: [VoucherService],
})
export class VoucherModule {}
