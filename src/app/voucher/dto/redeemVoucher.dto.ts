import { IsString } from 'class-validator';

export class RedeemVoucherDto {
  @IsString()
  key!: string;
}
