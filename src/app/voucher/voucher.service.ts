import { Logger, Injectable } from '@nestjs/common';
import { PrismaService } from '@app/database/prisma.service';
import { UserService } from '@app/user/user.service';
import { CreateTransactionDto } from '@app/transaction/dtos/createTransaction.dto';
import { transaction_type_enum, Voucher } from '@prisma/client';
import { TransactionService } from '@app/transaction/transaction.service';

@Injectable()
export class VoucherService {
  private readonly logger = new Logger(VoucherService.name);

  constructor(
    private readonly prismaService: PrismaService,
    private readonly userService: UserService,
    private readonly transactionService: TransactionService,
  ) {
    this.logger.log('Initiated');
  }

  async RedeemVoucher(sub: string, key: string) {
    let voucher: Voucher;

    try {
      voucher = await this.prismaService.voucher.findFirst({
        where: {
          key: key,
          redeemed: false,
          expires: {
            gte: new Date(),
          },
        },
      });
    } catch (err) {
      throw Error('Error fetching voucher');
    }
    if (!voucher) throw Error('Voucher not found');

    this.logger.log(`Voucher found: ${voucher.id}`);

    // Initiate new transaction
    const to = await this.userService.getByKratosId(sub);
    if (!to) {
      throw new Error('User not found');
    }

    // Create instance
    const transactionData: CreateTransactionDto = {
      to: to,
      from: undefined,
      type: transaction_type_enum.VOUCHER,
      amount: voucher.amount,
      description: 'Voucher ' + voucher.id,
    };
    Logger.debug({
      message: 'voucher transactiondata created',
      data: transactionData,
    });

    Logger.debug('Creating new voucher db entity');
    const newTransaction = await this.transactionService.create(transactionData);
    Logger.debug({ message: 'newTransactioncreated', data: newTransaction });

    Logger.debug('Setting voucher status to redeemed. ');
    try {
      await this.prismaService.voucher.update({
        where: { id: voucher.id },
        data: { redeemed: true, transactions: { connect: { id: newTransaction.id } } },
      });
    } catch (err) {
      throw new Error('Voucher status updated');
    }

    return voucher;
  }
}
