import { Escape, Trim } from 'class-sanitizer';
import { Expose } from 'class-transformer';
import { IsBoolean, IsDate, IsNumber, IsOptional, IsPositive, IsString, Length, MaxLength } from 'class-validator';

export class UpdateUserDto {
  @Expose()
  @IsOptional()
  @IsBoolean()
  email_verified?: boolean;

  @Expose()
  @IsOptional()
  @IsString()
  @Length(3, 25)
  @Escape()
  @Trim()
  nickname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @Escape()
  @Trim()
  picture?: string;

  @Expose()
  @IsOptional()
  @IsString()
  account_plan?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  @IsOptional()
  account_plan_end?: Date;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(15)
  @Escape()
  @Trim()
  firstname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(20)
  @Escape()
  @Trim()
  lastname?: string;

  @Expose()
  @IsOptional()
  @IsString()
  @MaxLength(300)
  @Trim()
  @Escape()
  bio?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  signed_up?: Date;

  @Expose()
  @IsOptional()
  @IsDate()
  last_email_notification?: Date;

  @Expose()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  notification_schedule?: number;
}
