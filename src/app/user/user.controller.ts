import { Controller, Get, Logger, Req, HttpException, HttpStatus, UseInterceptors } from '@nestjs/common';
import { UserService } from '@app/user/user.service';
import { BalanceService } from '@app/balance/balance.service';
import { TransactionService } from '@app/transaction/transaction.service';
import { SentryInterceptor } from '@utils/sentry.interceptor';
import { ApiCookieAuth, ApiTags } from '@nestjs/swagger';

export interface RequestInterface extends Request {
  user: { sub: string };
  query?: { nickname: string };
}

@ApiCookieAuth()
@ApiTags('user')
@Controller('user')
export class UserController {
  private readonly logger = new Logger(UserController.name);

  constructor(
    private readonly userService: UserService,
    private readonly balanceService: BalanceService,
    private readonly transactionService: TransactionService,
  ) {}

  @Get('me')
  async getCurrentUser(@Req() request: RequestInterface) {
    const { user } = request;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result;

    try {
      const account = await this.userService.getByKratosId(user.sub);
      if (!account) return new HttpException('User not found', HttpStatus.NOT_FOUND);
      const balance = await this.balanceService.current(user.sub);
      // const transactions = await this.transactionService.find(user.sub);

      result = { account, balance };
    } catch (err) {
      Logger.warn(err);
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  @Get('search')
  async searchUser(@Req() req: RequestInterface) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    const { query } = req;
    if (!query || !query.nickname) return new HttpException('No nickname povided', HttpStatus.BAD_REQUEST);

    let result;
    try {
      result = await this.userService.search(query.nickname, user.sub);
    } catch (err) {
      Logger.warn(err);
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }
}
