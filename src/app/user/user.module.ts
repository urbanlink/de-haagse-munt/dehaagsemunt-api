import { Module } from '@nestjs/common';
import { UserController } from '@app/user/user.controller';
import { UserService } from '@app/user/user.service';
import { BalanceModule } from '@app/balance/balance.module';
import { TransactionModule } from '@app/transaction/transaction.module';

@Module({
  imports: [BalanceModule, TransactionModule],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
