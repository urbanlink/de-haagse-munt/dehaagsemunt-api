import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '@app/database/prisma.service';
import { UpdateUserDto } from './dtos/updateUser.dto';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);

  constructor(private readonly prismaService: PrismaService) {}

  async getByEmail(email: string) {
    return await this.prismaService.user.findFirst({
      where: { email },
    });
  }

  async getById(id: number) {
    const user = await this.prismaService.user.findFirst({
      where: { id },
    });
    if (!user) throw new HttpException('Gebruiker niet gevonden', HttpStatus.NOT_FOUND);

    return user;
  }

  async getByKratosId(id: string) {
    const user = await this.prismaService.user.findFirst({ where: { kratosId: id } });
    if (!user) throw new HttpException('Gebruiker niet gevonden', HttpStatus.NOT_FOUND);

    return user;
  }

  async update(id: number, data: UpdateUserDto) {
    const result = await this.prismaService.user.update({
      where: { id: id },
      data,
    });
    return result;
  }

  async me(kratosId: string) {
    return this.prismaService.user.findFirst({ where: { kratosId } });
  }

  async search(nickname: string, sub: string) {
    return await this.prismaService.user.findMany({
      where: {
        nickname: { contains: nickname },
        NOT: { kratosId: sub },
      },
      select: { nickname: true, picture: true, kratosId: true },
      take: 20,
    });
  }
}
