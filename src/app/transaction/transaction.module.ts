import { Module } from '@nestjs/common';
import { TransactionController } from '@app/transaction/transaction.controller';
import { TransactionService } from '@app/transaction/transaction.service';
import { TransactionWorkerService } from '@app/transaction/transaction.worker';
import { BalanceModule } from '@app/balance/balance.module';

@Module({
  imports: [BalanceModule],
  controllers: [TransactionController],
  providers: [TransactionWorkerService, TransactionService],
  exports: [TransactionService, TransactionWorkerService],
})
export class TransactionModule {}
