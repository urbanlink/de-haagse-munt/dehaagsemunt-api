import { ApiProperty } from '@nestjs/swagger';
import { transaction_type_enum } from '@prisma/client';
import { IsEnum, IsNumber, IsString, Length, Min, Max, ValidateNested } from 'class-validator';
import { UserInTransactionDto } from './userInTransaction.dto';

export class CreateTransactionDto {
  @ApiProperty({ description: 'The amount', minimum: 1, default: 1 })
  @IsNumber()
  @Min(1)
  @Max(10000)
  amount!: number;

  @ApiProperty()
  @IsString()
  @Length(3, 150)
  description?: string;

  @ApiProperty()
  @ValidateNested()
  from?: UserInTransactionDto;

  @ApiProperty()
  @ValidateNested()
  to!: UserInTransactionDto;

  @ApiProperty({ name: 'type', enum: transaction_type_enum })
  @IsEnum(transaction_type_enum)
  type: transaction_type_enum;
}
