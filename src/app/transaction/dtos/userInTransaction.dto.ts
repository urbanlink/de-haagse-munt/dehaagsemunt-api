import { IsNumber } from 'class-validator';

export class UserInTransactionDto {
  @IsNumber()
  id!: number;
}
