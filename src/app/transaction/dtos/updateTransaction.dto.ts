import { transaction_type_enum } from '@prisma/client';
import { IsEnum, IsNumber, IsString, Length, Max, Min, ValidateNested } from 'class-validator';
import { UserInTransactionDto } from './userInTransaction.dto';

export class UpdateTransactionDto {
  @IsNumber()
  @Min(1)
  @Max(10000)
  amount!: number;

  @IsString()
  @Length(3, 150)
  description?: string;

  @ValidateNested()
  from?: UserInTransactionDto;

  @ValidateNested()
  to!: UserInTransactionDto;

  @IsEnum(transaction_type_enum)
  type: transaction_type_enum;
}
