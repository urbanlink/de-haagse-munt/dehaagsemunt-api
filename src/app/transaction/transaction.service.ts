import { Logger, Injectable } from '@nestjs/common';
import { BalanceService } from '@app/balance/balance.service';
import { PrismaService } from '@app/database/prisma.service';
import { CreateTransactionDto } from '@app/transaction/dtos/createTransaction.dto';
import { UpdateTransactionDto } from '@app/transaction/dtos/updateTransaction.dto';
import { Balance } from '@prisma/client';
import { CreateBalanceDto } from '@app/balance/dtos/createBalance.dto';
import { ChangeBalanceInterface } from '@app/balance/dtos/balance.interface';

class PagerOptions {
  take: number;
  skip: number;
}

@Injectable()
export class TransactionService {
  private logger = new Logger(TransactionService.name);

  constructor(private readonly prismaService: PrismaService, private readonly balanceService: BalanceService) {}

  async findOne(id: number) {
    return await this.prismaService.transaction.findFirst({
      where: { id },
    });
  }

  async find(sub: string, pager: PagerOptions = null) {
    if (!pager) {
      pager = {
        take: 10,
        skip: 0,
      };
    }

    const query = `
      SELECT
        t.*,
        b.amountmeta,
        u.id as user_id,
        from_id.nickname as from_nickname,
        from_id.picture as from_picture,
        to_id.nickname as to_nickname,
        to_id.picture as to_picture
      FROM public.user u
      JOIN balance b
        on u.id = b.user_id
      JOIN transaction t ON
        b.transaction_id = t.id
      LEFT OUTER JOIN public.user from_id ON
        from_id.id = t.from_id
      LEFT OUTER JOIN public.user to_id ON
        to_id.id = t.to_id
      WHERE
        u.auth0 = $1
      AND
        ( t.from_id=u.id OR t.to_id=u.id)
      ORDER BY t.id DESC
      LIMIT ${pager.take}
      OFFSET ${pager.skip}`;

    return await this.prismaService.transaction.findMany();
  }

  async create(data: CreateTransactionDto) {
    return await this.prismaService.transaction.create({ data });
  }

  async update(id: number, data: UpdateTransactionDto) {
    return await this.prismaService.transaction.update({ where: { id }, data });
  }

  // Find the next oldest (next) transaction in need of processing
  async findNextTransaction() {
    return await this.prismaService.transaction.findFirst({
      where: { processed: false },
      orderBy: { id: 'asc' },
    });
  }

  /**
   *
   * Process a transaction
   *
   * type <string> : transactiontype
   * amount: <number> : transaction amount
   * fromBalance: <Balance> : current balance of sender
   * toBalance: <Balance> : current balance of receiver
   * changeMeta: <iBalanceChange> : array of amountmeta changes
   *
   * calculateFundsAvailable():<boolean>
   * calculate new fromBalance: from: old: [], change: [], new: [] <ChangeBalanceInterface>
   * bump change_meta: []  <iAmountMeta>
   * calculate new toBalance : from: old: [], change: [], new: [] <ChangeBalanceInterface>
   *
   * Save newFromBalance
   * Save newToBalance
   * Update transaction status
   *
   * @param transaction
   *
   */
  async process(transaction: any) {
    if (!transaction) {
      return Promise.reject(new Error('No transaction provided'));
    }

    const type: string = transaction.type;
    const amount: number = transaction.amount;
    let fromBalance: Balance | undefined;
    let toBalance: Balance;
    let newFromBalanceDto: CreateBalanceDto;
    // let newFromBalance: Balance | undefined;
    let bumpedMeta: number[] = [amount];
    // let newBalanceDto: CreateBalanceDto;
    // let newToBalance: Balance;

    // Get current balance for sender
    if (['transaction', 'withdrawl'].indexOf(type)) {
      try {
        fromBalance = await this.balanceService.current(String(transaction.from.auth0));
        if (!fromBalance) {
          return Promise.reject(new Error('No transaction provided'));
        }
      } catch (err) {
        return Promise.reject('No current from balance found');
      }
    }

    // Validate if sender has enough funds available
    if (fromBalance) {
      if (transaction.from && (fromBalance.amount || 0) < transaction.amount) {
        return Promise.reject(new Error('No sufficient funds for the transaction'));
      }
    }

    // Get current balance for recipient
    try {
      const b = await this.balanceService.current(String(transaction.to.auth0));
      if (!b) {
        return Promise.reject(new Error('No current to balance'));
      }
      toBalance = b;
    } catch (err) {
      return Promise.reject(err);
    }

    // Calculate new sender's balance
    if (fromBalance) {
      const b: ChangeBalanceInterface = this.balanceService.SubtractBalanceMeta(fromBalance.amountMeta, amount);
      newFromBalanceDto = {
        user: transaction.from,
        transaction: transaction,
        amountmeta: b.new,
      };
      // calculate bumped metadata
      bumpedMeta = this.balanceService.BumpMetaData(b.change);
      // save the new balance
      await this.balanceService.create(newFromBalanceDto);
    }

    // calculate new recipient's balance
    const newToBalanceMeta: number[] = this.balanceService.AddBalanceMeta(toBalance.amountMeta, bumpedMeta);
    const newToBalanceDto: CreateBalanceDto = {
      user: transaction.to,
      amountmeta: newToBalanceMeta,
      transaction: transaction,
    };
    // save the new balance
    await this.balanceService.create(newToBalanceDto);

    // Save the final transaction
    transaction.processed = true;
    transaction.status = true;
    await this.prismaService.transaction.create(transaction);

    return true;
  }
}
