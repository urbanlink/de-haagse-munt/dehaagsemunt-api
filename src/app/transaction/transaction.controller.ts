import { Controller, Get, Post, Body, Param, Req, HttpException, HttpStatus } from '@nestjs/common';
import { TransactionService } from '@app/transaction/transaction.service';
import { TransactionWorkerService } from '@app/transaction/transaction.worker';
import { ApiCookieAuth, ApiTags } from '@nestjs/swagger';
import { CreateTransactionDto } from './dtos/createTransaction.dto';

export interface RequestInterface extends Request {
  user: { sub: string };
  query?: {
    take: number;
    skip: number;
  };
}

@ApiCookieAuth()
@ApiTags('transaction')
@Controller('transaction')
export class TransactionController {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionWorker: TransactionWorkerService,
  ) {}

  @Post()
  async create(@Body() transactionDto: CreateTransactionDto, @Req() req: RequestInterface) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: any;
    try {
      result = await this.transactionService.create(transactionDto);
    } catch (err) {
      return new HttpException('Error creating new transaction', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // Notify the transactionworker to process the result
    this.transactionWorker.Ping();

    return result;
  }

  @Get()
  async findAll(@Req() req: RequestInterface) {
    const { user, query } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: any;
    const options = {
      take: Number(query.take) || 10,
      skip: Number(query.skip) || 0,
    };

    try {
      result = await this.transactionService.find(user.sub, options);
    } catch (err) {
      return new HttpException('Error fetching transactions list', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  @Get(':id')
  async findOne(@Req() req: RequestInterface, @Param('id') id: string) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: any;
    try {
      this.transactionService.findOne(+id);
    } catch (err) {
      return new HttpException('Error fetching transactions list', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }
}
