import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { SentryInterceptor } from '@utils/sentry.interceptor';

@Controller()
export class AppController {
  @Get()
  @UseInterceptors(new SentryInterceptor())
  @ApiBearerAuth()
  home(): object {
    return {
      title: 'De Haagse Munt API',
      version: process.env.npm_package_version,
      environment: process.env.NODE_ENV,
    };
  }
}
