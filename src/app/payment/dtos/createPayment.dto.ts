import { IsString, IsNumber, IsPositive, IsInt } from 'class-validator';

export class CreatePaymentDto {
  @IsString()
  description!: string;

  @IsNumber()
  @IsPositive()
  @IsInt()
  amount!: number;

  @IsNumber()
  @IsPositive()
  @IsInt()
  costs!: number;

  @IsNumber()
  @IsPositive()
  user_id!: number;
}
