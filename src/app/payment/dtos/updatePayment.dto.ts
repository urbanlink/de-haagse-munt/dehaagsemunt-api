import { IsInt, IsNumber, IsPositive, IsString } from 'class-validator';

export class UpdatePaymentDto {
  @IsString()
  description?: string;

  @IsNumber()
  @IsPositive()
  @IsInt()
  amount?: number;

  @IsNumber()
  @IsPositive()
  @IsInt()
  costs?: number;

  @IsNumber()
  @IsPositive()
  user_id?: number;

  @IsString()
  status?: string;

  @IsString()
  mollie_details?: string;
}
