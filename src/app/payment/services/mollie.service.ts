import { Logger } from '@nestjs/common';

export class MollieService {
  private mollie: any;

  /**
   *
   * Fetch a payment from Mollie
   * @param id
   */
  GetPayment(id: string) {
    return new Promise((resolve, reject) => {
      this.mollie.payments.get(id, (result: any) => {
        Logger.debug(result);
        if (result.error) {
          Logger.debug('[ mollie.service ] - There was an error! ', result.error);
          reject(result.error);
        } else {
          resolve(result);
        }
      });
    });
  }

  // Create a new payment
  CreatePayment(data: any) {
    return new Promise((resolve, reject) => {
      this.mollie.payments.create(data, (result: any) => {
        Logger.debug(result);
        if (result.error) {
          Logger.debug('[ mollie.service ] - There was an error! ', result.error);
          reject(result.error);
        } else {
          resolve(result);
        }
      });
    });
  }
}
