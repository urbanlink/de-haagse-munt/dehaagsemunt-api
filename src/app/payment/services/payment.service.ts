import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MollieService } from '@app/payment/services/mollie.service';
import { PrismaService } from '@app/database/prisma.service';
import { Payment } from '@prisma/client';
import { CreatePaymentDto } from '../dtos/createPayment.dto';
import { UpdatePaymentDto } from '../dtos/updatePayment.dto';

export class PaymentService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly configService: ConfigService,
    private readonly mollieService: MollieService,
  ) {}

  async findOne(mollieId: string) {
    return await this.prismaService.payment.findFirst({
      where: {
        mollieId,
      },
    });
  }

  async findMany(sub: number) {
    return await this.prismaService.payment.findMany({
      where: {
        userId: sub,
      },
      orderBy: { id: 'desc' },
    });
  }

  async create(payment: CreatePaymentDto) {
    Logger.log('[ payment.controller ] - Starting to create a new payment.' + JSON.stringify(payment));

    // Save payment in db with status -in-progress
    const newPayment = {
      description: 'Inleg De Haagse Munt',
      amount: payment.amount,
      costs: payment.costs,
      status: 'draft',
      userId: payment.user_id,
    };
    Logger.log('[ payment.controller ] - Creating new payment: ', JSON.stringify(newPayment));

    try {
      const payment = await this.prismaService.payment.create({ data: newPayment });
      if (!payment) {
        return null;
      } //res.status(404).json({ msg: 'Payment not created', value: payment} ); }
      Logger.log('[ payment.controller ] - Payment created. Sending payment to mollie.');

      const mollieData = {
        amount: (payment.amount + payment.costs) / 100,
        description: 'Inleg De Haagse Munt: Order id: ' + payment.id,
        redirectUrl: this.configService.get('DOMAIN') + '/dashboard/payments',
        webhookUrl: this.configService.get('MOLLIE_WEBHOOK'),
        metadata: {
          // eslint-disable-next-line @typescript-eslint/camelcase
          payment_id: payment.id,
        },
      };
      Logger.debug('[ payment.controller ] - New mollie payment: ', JSON.stringify(mollieData));
      // const molliePayment: Payment = await this.mollieService.CreatePayment(mollieData);
      // Logger.debug('[ payment.controller ] - Payment received by Mollie: ', JSON.stringify(molliePayment));

      // const updateData = {
      //   status: molliePayment.status,
      //   // eslint-disable-next-line @typescript-eslint/camelcase
      //   mollie_id: molliePayment.id,
      //   // eslint-disable-next-line @typescript-eslint/camelcase
      //   mollie_details: JSON.stringify(molliePayment)
      // };
      // const result = payment. .save(updateData);
      // if (result.error) {
      //   Logger.warn("[ payment.controller ] - ", result);
      //   return result;
      // }

      // return molliePayment;
    } catch (err) {
      return err;
    }
  }

  async UpdatePayment(id: number, data: UpdatePaymentDto): Promise<boolean> {
    Logger.log(id, JSON.stringify(data));
    return false;
  }
}
