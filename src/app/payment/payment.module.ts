import { Module } from '@nestjs/common';
import { PaymentController } from '@app/payment/payment.controller';
import { PaymentService } from '@app/payment/services/payment.service';
import { MollieService } from '@app/payment/services/mollie.service';

@Module({
  controllers: [PaymentController],
  providers: [PaymentService, MollieService],
  exports: [PaymentService],
})
export class PaymentModule {}
