import { Controller, Get, Logger, Post, Body, Req, UseInterceptors, HttpException, HttpStatus } from '@nestjs/common';
import { PaymentService } from '@app/payment/services/payment.service';
import { MollieService } from '@app/payment/services/mollie.service';
import { SentryInterceptor } from '@utils/sentry.interceptor';
import { logger } from '@sentry/utils';
import { Payment } from '@prisma/client';
import { CreatePaymentDto } from './dtos/createPayment.dto';

export interface RequestInterface extends Request {
  user: { sub: string; id: number };
}

@UseInterceptors(SentryInterceptor)
@Controller('payment')
export class PaymentController {
  private logger = new Logger('PaymentController');

  constructor(private readonly paymentService: PaymentService, private readonly mollieService: MollieService) {
    this.logger.log('Initiating PaymentController');
  }

  /**
   *
   * Get a list of payments for current user.
   *
   **/
  @Get()
  async getPayments(@Req() req: RequestInterface) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: Payment[];
    try {
      result = await this.paymentService.findMany(user.id);
    } catch (err) {
      logger.error(JSON.stringify(err));
      return new HttpException('Error fetching payments list', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }

  /**
   *
   * Public hook for Mollie updates.
   *
   **/
  @Get('mollie-hook')
  async mollieHook(@Req() req: RequestInterface, @Body() id: string): Promise<boolean | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);
    const mollieId = id;
    if (!mollieId) return new HttpException('No mollieId povided', HttpStatus.BAD_REQUEST);

    // Fetch the payment from mollie with the provided mollie_id
    try {
      const molliePayment = await this.mollieService.GetPayment(mollieId);
      logger.log('JSON.stringify(molliePayment');
      // const mollieStatus = String(molliePayment.status);
      const payment = await this.paymentService.findOne(mollieId);

      if (!payment) {
        logger.log(`The requested payment was not found! ${mollieId}`);
        return new HttpException('Payment not found', HttpStatus.NOT_FOUND);
      }

      const localStatus = String(payment.status);
      // logger.log(`Updating status: ${localStatus} to ${mollieStatus}`);

      // Update the payment status in the db
      // await this.paymentService.UpdatePayment(payment.id, {
      //   status: mollieStatus,
      //   // eslint-disable-next-line @typescript-eslint/camelcase
      //   mollie_details: JSON.stringify(molliePayment),
      // });
    } catch (err) {
      logger.error(JSON.stringify(err));
      return new HttpException('Error updating payment status', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return true;
  }

  /**
   *
   * Create a new payment by current user.
   *
   **/
  @Post()
  async createPayment(@Body() payment: CreatePaymentDto): Promise<Payment | HttpException> {
    let result: Payment;
    try {
      result = await this.paymentService.create(payment);
    } catch (err) {
      logger.error(JSON.stringify(err));
      return new HttpException('Error creating new payment', HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return result;
  }
}
