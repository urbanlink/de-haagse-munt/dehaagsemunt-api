import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from '@app/user/user.module';
import { kratosSession } from '@app/auth/middlewares/session.middleware';

@Module({
  imports: [ConfigModule, UserModule],
})
export class AuthModule implements NestModule {
  // Add Kratos session middleware
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(kratosSession).forRoutes('*');
  }
}
