import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Session } from '@ory/kratos-client';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const request = this.getRequest(context);
    if (!request.session) return false;

    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) return true;
    return this.matchRoles(roles, request.session);
  }

  getRequest<T = any>(context: ExecutionContext): T {
    return context.switchToHttp().getRequest();
  }

  matchRoles = (roles: string[], session: Session) => {
    if (session.identity?.traits?.email === process.env.ADMIN_EMAIL) return true;
  };
}
