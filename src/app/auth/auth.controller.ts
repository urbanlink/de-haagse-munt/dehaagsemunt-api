import { ClassSerializerInterceptor, Controller, HttpCode, Post, Req, UseInterceptors } from '@nestjs/common';
import { Request } from 'express';
import axios from 'axios';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {
  constructor() {}

  @HttpCode(200)
  @Post('session')
  current(@Req() request: Request) {
    // Make a request and include the cookie in X-Session-Cookie
    try {
      axios
        .get(`/api/.ory/sessions/whoami`, {
          // headers: { 'X-Session-Cookie': cookie },
        })
        .then(session => {
          return session;
        });
    } catch (err) {
      console.warn(err);
    }
  }
}
