import { Configuration, Session, V0alpha2Api } from '@ory/kratos-client';
import { NextFunction, Request, Response } from 'express';

export interface RequestWithSession extends Request {
  session: Session;
}

const kratos = new V0alpha2Api(new Configuration({ basePath: process.env.KRATOS_BASE_URL }));

// Add Kratos session to request if cookie or bearer token is present
export const kratosSession = async (req: RequestWithSession, res: Response, next: NextFunction) => {
  const cookie: string = req.cookies['ory_kratos_session'] || req.headers.cookie || null;
  let session_token = req.headers.authorization?.split(' ')[1];
  let data: Session;

  if (cookie) {
    const result = await kratos.toSession(null, `ory_kratos_session=${cookie}`);
    data = result.data;
  } else if (session_token) {
    const result = await kratos.toSession(session_token, null);
    data = result.data;
  }

  if (data) req.session = data;

  next();
};
