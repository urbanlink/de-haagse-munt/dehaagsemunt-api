# Notification Module

The notifications module is in charge of sending notifications to users. 

At this point there is one notification: Email message

## Notification: Schedules notification email for new messages

  unreadMessagesEmailNotification

  - Find all users with unread messages within users notification schedule
  - Send a new notification mail to each user 

  message.read_status               boolean   Did the user read this message? 
  message.created_at                date      Datetime when the message was created 
  message.user_id                   int       Internal id of the user 

  user.notification_schedule        int       How much time between notifications, in minutes. 
                                              (direct (=0), hour, day, week, never (null))
  user.last_email_notification      date      Last time a notification was send to the user


  SELECT (id, email, nickname) from user 
  LEFTJOIN COUNT(message.id) on message.user_id = user.id
  WHERE 
    message.read_status = false
    AND user.digest_schedule NOT IS NULL 
    AND user.digest_schedule > 0
    AND message.last_digest + user.digest_schedule > now()