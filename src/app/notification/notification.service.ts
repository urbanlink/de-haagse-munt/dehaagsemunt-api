import { Logger, Injectable } from '@nestjs/common';
import { MessageService } from '@app/message/message.service';
import { EmailService } from '@app/email/email.service';
import { PrismaService } from '@app/database/prisma.service';
import { User } from '@prisma/client';

@Injectable()
export class NotificationService {
  private readonly logger = new Logger(NotificationService.name);

  constructor(
    private readonly prismaService: PrismaService,
    private readonly messageService: MessageService,
    // private readonly userService: UserService,
    private readonly emailService: EmailService,
  ) {
    this.logger.log('initiated');
  }

  /**
   *
   * Find all users with unread messages since last email send to the user
   *
   *
   */
  async HandleUnreadMessagesEmailNotification() {
    // //   /*
    // //   SELECT u.id, u.email, u.nickname, count(m.id) as count FROM public.user AS u
    // //   LEFT JOIN message AS m ON m.user_id = u.id
    // //   WHERE
    // //           m.read_status = FALSE
    // //     AND u.email_digest_schedule > 0
    // //     AND u.last_email_digest > (now() - u.email_digest_schedule). [todo]
    // //     AND m.created_at > (now() - u.email_digest_schedule) [todo]
    // //   GROUP BY u.id;
    // //   */
    // //   const m = await getRepository(User)
    // //     .createQueryBuilder('u')
    // //     .select('COUNT(*) AS message_count')
    // //     .from(Message, 'message')
    // //     .leftJoin('message.user', 'user')
    // //     .addSelect(['user.id', 'user.auth0', 'user.email', 'user.nickname', 'user.last_email_digest'])
    // //     .where('message.emailed_status = :status', { status: false })
    // //     .andWhere(
    // //       new Brackets(qb => {
    // //         qb
    // //           .where('user.last_email_digest < :digestDate')
    // //           .orWhere('user.last_email_digest IS NULL');
    // //       }),
    // //     )
    // //     .setParameters(filters)
    // //     .groupBy('user.id')
    // //     .getRawMany();
    // return await getRepository(User)
    //   .createQueryBuilder('u')
    //   .select(['id', 'email', 'nickname'])
    //   .leftJoinAndSelect('m.user', 'm')
    //   .addSelect('COUNT(m.id)', 'count')
    //   .where('m.read_status = :status', { status: false })
    //   .andWhere('u.email_digest_schedule > 0')
    //   .setParameter('notificationSchedule', 'u.notification_schedule')
    //   .setParameter('lastEmailNotification', 'u.last_email_notification')
    //   // .andWhere('u.last_email_digest > :time', { time: (Date() - u.email_digest_schedule))
    //   // .andWhere( m.created_at > (Date() - u.email_digest_schedule) )
    //   .groupBy('u.id')
    //   .execute();
  }

  /**
   *
   *
   *
   */
  async SendNotifications() {
    try {
      // Get all unsentMessages orderd by user.
      this.logger.log(`Fetching all unread messages by user since last digest_date`);
      const m: any = await this.messageService.GetUnsentMessagesByUser();
      this.logger.log(`Found ${m.length} users with unsent messages`);

      // Send an email with notification to each user.
      m.map(async (u: any, index: number) => {
        this.logger.log(`Send notification ${index}: ${u.user_nickname}`);
        await this.emailService.SendEmail({
          type: 'messageReceived',
          nickname: u.user_nickname,
          to: u.user_email,
          messageCount: u.message_count,
        });
        this.logger.log(`Notification email sent to user`);
        // eslint-disable-next-line @typescript-eslint/camelcase
        // await this.userService.UpdateUser(u.sub, { last_email_digest: new Date() });
      });
    } catch (err) {
      this.logger.warn(err.toString());
    }
  }
}
