export class IEmailNotificationData {
  user_id?: number;
  user_auth0?: string;
  email?: string;
  nickname?: string;
  messageCount?: number;
}
