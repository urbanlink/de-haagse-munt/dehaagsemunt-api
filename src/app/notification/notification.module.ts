import { Module } from '@nestjs/common';
import { NotificationService } from '@app/notification/notification.service';
import { MessageModule } from '@app/message/message.module';
import { UserModule } from '@app/user/user.module';
import { EmailModule } from '@app/email/email.module';

@Module({
  imports: [MessageModule, UserModule, EmailModule],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationsModule {}
