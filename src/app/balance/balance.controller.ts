import { Controller, Get, Req, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { BalanceService } from '@app/balance/balance.service';
import { Balance } from '@prisma/client';

export interface RequestInterface extends Request {
  user: {
    sub: string;
  };
}

@Controller('balance')
export class BalanceController {
  private logger = new Logger(BalanceController.name);

  constructor(private readonly balanceService: BalanceService) {
    this.logger.log('Initiating BalanceController');
  }

  @Get()
  async getBalanceList(@Req() req: RequestInterface) {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    return await this.balanceService.BalanceList(user.sub);
  }

  @Get('/current')
  async getBalance(@Req() req: RequestInterface): Promise<Balance | HttpException> {
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    let result: Balance;
    try {
      result = await this.balanceService.current(user.sub);
    } catch (err) {
      return new HttpException('error fetching balance list', HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }
}
