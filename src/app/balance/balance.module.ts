import { Module } from '@nestjs/common';
import { BalanceController } from '@app/balance/balance.controller';
import { BalanceService } from '@app/balance/balance.service';

@Module({
  controllers: [BalanceController],
  providers: [BalanceService],
  exports: [BalanceService],
})
export class BalanceModule {}
