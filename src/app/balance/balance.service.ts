import { PrismaService } from '@app/database/prisma.service';
import { Injectable } from '@nestjs/common';
import { CreateBalanceDto } from '@app/balance/dtos/createBalance.dto';
import { ChangeBalanceInterface } from '@app/balance/dtos/balance.interface';
import { Balance } from '@prisma/client';

@Injectable()
export class BalanceService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(balanceDto: CreateBalanceDto): Promise<Balance> {
    return await this.prismaService.balance.findFirst();
    // return await this.prismaService.balance.create({ data: balanceDto });
  }

  // Get current balance for a user
  async current(sub: string): Promise<Balance | undefined> {
    return await this.prismaService.balance.findFirst();
    // .createQueryBuilder('balance')
    // .leftJoin('balance.user', 'user')
    // .where('user.auth0=:sub', { sub: sub })
    // .orderBy('balance.id', 'DESC')
    // .getOne();
  }

  // Get a list of recent balances for a user
  async BalanceList(sub: string) {
    return await this.prismaService.balance.findFirst();
    // return await getRepository(Balance)
    //   .createQueryBuilder('balance')
    //   .leftJoin('balance.user', 'user')
    //   .leftJoinAndSelect('balance.transaction', 'transaction')
    //   .leftJoinAndSelect('transaction.from', 'from')
    //   .leftJoinAndSelect('transaction.to', 'to')
    //   .where('user.auth0 = :sub', { sub: sub })
    //   .orderBy('balance.id', 'DESC')
    //   .getMany();
  }

  /**
   *
   * Subtract an amount from a balance
   *
   * @param balanceMeta
   * @param amount
   */
  SubtractBalanceMeta(_meta: number[], _amount: number): ChangeBalanceInterface {
    let oldMeta: number[] = _meta;
    let changeMeta: number[] = [];
    let newMeta: number[] = [];
    let amountLeft = _amount;

    oldMeta = oldMeta.reverse();
    oldMeta.map((stepValue: number, index: number) => {
      const amountToSubtract = Math.min(amountLeft, stepValue);
      changeMeta[index] = amountToSubtract;
      newMeta[index] = oldMeta[index] - amountToSubtract;
      amountLeft = amountLeft - amountToSubtract;
    });

    oldMeta = oldMeta.reverse();
    changeMeta = changeMeta.reverse();
    newMeta = newMeta.reverse();

    return {
      old: oldMeta,
      change: changeMeta,
      new: newMeta,
    };
  }

  /**
   * @param metaData
   */
  BumpMetaData(metaData: number[]): number[] {
    metaData.unshift(0);
    metaData[metaData.length] += metaData[metaData.length + 1];
    metaData.splice(metaData.length, 1);

    return metaData;
  }

  /**
   *
   * @param meta
   * @param change
   */
  AddBalanceMeta(meta: number[], change: number[]): number[] {
    meta.map((item: number, index: number) => {
      meta[index] = (meta[index] || 0) + (change[index] || 0);
    });

    return meta;
  }
}
