export interface ChangeBalanceInterface {
  old: number[];
  change: number[];
  new: number[];
}
