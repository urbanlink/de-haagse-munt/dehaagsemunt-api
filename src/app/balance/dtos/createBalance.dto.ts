import { IsArray, ValidateNested } from 'class-validator';
import { TransactionInBalanceDto } from './transactionInBalance.dto';
import { UserInBalanceDto } from './userInBalance.dto';

export class CreateBalanceDto {
  @IsArray()
  amountmeta?: number[];

  @ValidateNested()
  user!: UserInBalanceDto;

  @ValidateNested()
  transaction?: TransactionInBalanceDto;
}
