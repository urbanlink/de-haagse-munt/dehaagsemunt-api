import { IsNumber } from 'class-validator';

export class TransactionInBalanceDto {
  @IsNumber()
  id!: number;
}
