import { IsNumber } from 'class-validator';

export class UserInBalanceDto {
  @IsNumber()
  id!: number;
}
