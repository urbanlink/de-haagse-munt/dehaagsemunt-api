export const environment = {
  production: false,
  env: process.env.NODE_ENV,
  port: parseInt(process.env.PORT),

  frontendUrl: process.env.FRONTEND_URL,
  backendUrl: process.env.BACKEND_URL,
  socketUrl: process.env.SOCKET_URL,

  sentryDsn: process.env.SENTRY_DSN,

  admin: {
    email: process.env.ADMIN_EMAIL,
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD,
  },
};
